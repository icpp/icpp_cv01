#include <iostream>
#include "Trojuhelnik.h";

bool lzeSestrojit(Trojuhelnik* _trojuhelnik){
    return _trojuhelnik->a+_trojuhelnik->b>_trojuhelnik->c
    &&_trojuhelnik->a+_trojuhelnik->c>_trojuhelnik->b
    &&_trojuhelnik->b+_trojuhelnik->c>_trojuhelnik->a;
}

int spocitejObvod(Trojuhelnik* _trojuhelnik){
    return _trojuhelnik->a+_trojuhelnik->b+_trojuhelnik->c;
}

int main() {
    int pocet; //pocetTrojuhelniku
    std::cout << "Zadejte prosim pocet trojuhelniku:";
    std::cin >> pocet;

    Trojuhelnik* trojuhelniky = new Trojuhelnik[pocet];
    for (int i = 0; i < pocet ; ++i) {
        std::cout << "Trojuhelnik c. " << i+1<< std::endl;
        std::cout << "Zadejte stranu A:";
        std::cin >> trojuhelniky[i].a;
        std::cout << "Zadejte stranu B:";
        std::cin >> trojuhelniky[i].b;
        std::cout << "Zadejte stranu C:";
        std::cin >> trojuhelniky[i].c;

        if(lzeSestrojit(&trojuhelniky[i])){
            std::cout << "Trojuhelnik lze sestrojit. Jeho obvod je: " << spocitejObvod(&trojuhelniky[i]) << std::endl;
        } else {
            std::cout << "Tento trojuhelník nelze sestrojit!" << std::endl;
        }
    }

    delete[] trojuhelniky;
    return 0;
}
